<?php
//
// child theme functions.php
//  
add_action( 'wp_enqueue_scripts', 'oculis_enqueue_styles' );
function oculis_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
	wp_enqueue_style('oculis-yellow', get_stylesheet_directory_uri() . '/css/yellow.css');
}