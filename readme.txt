=== Oculis ===
Contributors: weblizar
Tags: two-columns, custom-menu, right-sidebar, custom-background, featured-image-header, sticky-post, theme-options, threaded-comments, featured-images, flexible-header, translation-ready , blog  , custom-logo , E-Commerce , footer-widgets , portfolio
Requires at least: 4.0
Tested up to: 5.5
Requires PHP: 7.0
Stable tag: 1.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Oculis is a child theme of enigma theme, which is superfine and easy to customize theme.

== Description ==
Oculis is a child theme of most famous WordPress theme Enigma. It is modern and easy to use. It is fully responsive and multipurpose theme with the latest features. Oculis is the perfect theme for the professionals, bloggers and E-Commerce. It provides a clean and flexible design.

<a href="https://wordpress.org/themes/oculis/">Discover more about the theme</a>.

== Frequently Asked Questions ==

= Where can i raise the theme issue? =

Please drop your issues here <a href="https://wordpress.org/support/theme/oculis"> we'll try to triage issues reported on the theme forum, you'll get a faster response.

= Where can I read more about Oculis? =

<a href="https://wordpress.org/themes/oculis/">Theme detail page</a>

== Changelog ==

= 1.8 =
**Update according to latest enigma.
**Screenshot update


= 1.7.2 =
**Bug Fix( compatible to get_theme_mod() ).

= 1.7.1 =
**BS4 competible.

= 1.7 =
* Full width template added.

= 1.6 =
* Minor CSS Fix.

= 1.5 =
* Tested with latest WP.
* Minor css issue fixed.

= 1.4 =
* Read-me file update as per new rules.

= 1.3 =
* minor issues fixed.

= 1.2 =
* https://themes.trac.wordpress.org/ticket/59886#comment:2 issues fixed.

== Image Resources ==
= Screenshot =
* https://pxhere.com/en/photo/795312 | CC0 Public Domain
All images are licensed under [CC0] (https://creativecommons.org/publicdomain/zero/1.0/)